﻿using DiscountPublisher.Repository.Abstract;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DiscountPublisher.Repository.Concrete
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly IMongoCollection<TEntity> mongoDb;
        private MongoClientSettings connectionString;
        private string dbName = "DiscountPublisher";
        private string tableName = "Logs";
        private readonly IMongoDatabase database;
        private readonly IMongoCollection<TEntity> collection;
        public Repository()
        {
            var client = new MongoClient("mongodb://localhost:27017");

            database = client.GetDatabase(dbName);

            collection = database.GetCollection<TEntity>(typeof(TEntity).Name);
        }
        public async Task<bool> Add(TEntity entity)
        {
            try
            {
                SetBaseProperties(entity); 
                await collection.InsertOneAsync(entity);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<List<TEntity>> GetListByFilter(Expression<Func<TEntity, bool>> filter)
        {
            var result = await collection.Find(filter).ToListAsync();
            return result;
        }

        public async Task<List<TEntity>> GetListByFilterOrderByDesc(Expression<Func<TEntity, bool>> filter, int offset, int limit, Expression<Func<TEntity, object>> sort)
        {
            var result = await collection.Find(filter).SortByDescending(sort).Skip(offset * limit).Limit(limit).ToListAsync();
            return result;
        }

        public async Task<long> GetCountFilter(Expression<Func<TEntity, bool>> filter)
        {
            return await collection.CountDocumentsAsync(filter);
        }

        public async Task<bool> Delete(Expression<Func<TEntity, bool>> filter)
        {
            await collection.DeleteManyAsync(filter);
            return true;
        }

        public async Task<bool> Update(Expression<Func<TEntity, bool>> filter, TEntity entity)
        {
            await collection.ReplaceOneAsync(filter, entity);
            return true;
        }


        private void SetBaseProperties(TEntity entity)
        {
            Type type = entity.GetType();

            PropertyInfo prop = type.GetProperty("CreateDate");

            prop.SetValue(entity, DateTime.Now, null);
        }
    }
}
