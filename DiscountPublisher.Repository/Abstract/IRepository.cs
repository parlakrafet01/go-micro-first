﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DiscountPublisher.Repository.Abstract
{
    public interface IRepository<TEntity> where TEntity : class
    {
        Task<bool> Add(TEntity entity);

        Task<List<TEntity>> GetListByFilter(Expression<Func<TEntity, bool>> filter);
        Task<List<TEntity>> GetListByFilterOrderByDesc(Expression<Func<TEntity, bool>> filter, int offset, int limit, Expression<Func<TEntity, object>> sort);

        Task<long> GetCountFilter(Expression<Func<TEntity, bool>> filter);
        Task<bool> Delete(Expression<Func<TEntity, bool>> filter);
        Task<bool> Update(Expression<Func<TEntity, bool>> filter, TEntity entity);
    }
}
