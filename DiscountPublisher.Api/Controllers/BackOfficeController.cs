﻿using DiscountPublisher.Business.Abstract;
using DiscountPublisher.DataTransferObjects.Base;
using DiscountPublisher.DataTransferObjects.Brand;
using DiscountPublisher.DataTransferObjects.Notification;
using DiscountPublisher.DataTransferObjects.Report;
using DiscountPublisher.DataTransferObjects.StoryContent.Request;
using DiscountPublisher.DataTransferObjects.TimeLineContent.Request;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DiscountPublisher.Api.Controllers
{
    [Route("api/backoffice")]
    public class BackOfficeController : ControllerBase
    {
        private readonly ITimeLineBusinessManager _timeLineBusinessManager;
        private readonly IStoryBusinessManager _storyBusinessManager;
        private readonly IBrandBusinessManager _brandBusinessManager;
        private readonly IReportBusinessManager _reportsBusinessManager;
        private readonly INotificationBusinessManager _notificationBusinessManager;
        public BackOfficeController(ITimeLineBusinessManager timeLineBusinessManager,
            IStoryBusinessManager storyBusinessManager,
            IBrandBusinessManager brandBusinessManager,
            IReportBusinessManager reportBusinessManager,
            INotificationBusinessManager notificationBusinessManager)
        {
            _timeLineBusinessManager = timeLineBusinessManager;
            _storyBusinessManager = storyBusinessManager;
            _brandBusinessManager = brandBusinessManager;
            _reportsBusinessManager = reportBusinessManager;
            _notificationBusinessManager = notificationBusinessManager;
        }

        #region backofficeMethods

        #region posts
        
        /// <summary>
        /// Post eklemek için kullanılır.
        /// </summary>
        /// <returns></returns>
        [HttpPost("createTimeLineContent"), DisableRequestSizeLimit]
        public async Task<IActionResult> CreateTimeLineContent([FromForm] CreateTimeLineContentRequest request)
        {
            return Ok(await _timeLineBusinessManager.CreateTimeLineContent(request));
        }

        /// <summary>
        /// Post güncellemek için kullanılır.
        /// </summary>
        /// <returns></returns>
        [HttpPost("updateTimeLineContent"), DisableRequestSizeLimit]
        public async Task<IActionResult> UpdateTimeLineContent([FromForm] UpdateTimeLineContentRequest request)
        {
            return Ok(await _timeLineBusinessManager.UpdateTimeLineContent(request));
        }


        /// <summary>
        /// Post silmek için kullanılır.
        /// </summary>
        /// <returns></returns>
        [HttpPost("deleteTimeLineContent")]
        public async Task<IActionResult> DeleteTimeLineContent([FromBody] DeleteBrandRequest request)
        {
            return Ok(await _timeLineBusinessManager.DeleteTimeLineContent(request.Id));
        }



        /// <summary>
        /// Post aktivasyonunu değiştirmek için kullanılır için kullanılır.
        /// </summary>
        /// <returns></returns>
        [HttpPost("updatePostActive"), DisableRequestSizeLimit]
        public async Task<IActionResult> UpdatePostActive([FromBody] UpdatePostActiveRequest request)
        {
            return Ok(await _timeLineBusinessManager.UpdateTimeLineContentActive(request));
        }

        /// <summary>
        /// Timeline listesini almak için başvurulur .
        /// </summary>
        /// <returns>TimeLine içerik listesini döner</returns>
        /// 
        [Authorize]
        [HttpGet]
        [Route("getPosts")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(IEnumerable<GetPostsRequest>))]
        public async Task<IActionResult> GetPosts([FromQuery] GetPostsRequest request)
        {
            return Ok(await _timeLineBusinessManager.GetPosts(request));
        }
        #endregion posts

        #region stories
        /// <summary>
        /// Story eklemek için kullanılır.
        /// </summary>
        /// <returns></returns>
        [HttpPost("createStoryContent"), DisableRequestSizeLimit]
        public async Task<IActionResult> CreateStoryContent([FromForm] CreateStoryContentRequest request)
        {
            return Ok(await _storyBusinessManager.CreateStoryContent(request));
        }

        /// <summary>
        /// Post güncellemek için kullanılır.
        /// </summary>
        /// <returns></returns>
        [HttpPost("updateStoryContent"), DisableRequestSizeLimit]
        public async Task<IActionResult> UpdateStoryContent([FromForm] UpdateStoryContentRequest request)
        {
            return Ok(await _storyBusinessManager.UpdateStoryContent(request));
        }

        /// <summary>
        /// Post silmek için kullanılır.
        /// </summary>
        /// <returns></returns>
        [HttpPost("deleteStoryContent")]
        public async Task<IActionResult> DeleteStoryContent([FromBody] DeleteBrandRequest request)
        {
            return Ok(await _storyBusinessManager.DeleteStoryContent(request.Id));
        }



        /// <summary>
        /// Post aktivasyonunu değiştirmek için kullanılır için kullanılır.
        /// </summary>
        /// <returns></returns>
        [HttpPost("updateStoryActive"), DisableRequestSizeLimit]
        public async Task<IActionResult> UpdateStoryContentActive([FromBody] UpdatePostActiveRequest request)
        {
            return Ok(await _storyBusinessManager.UpdateStoryContentActive(request));
        }

        /// <summary>
        /// Timeline listesini almak için başvurulur .
        /// </summary>
        /// <returns>TimeLine içerik listesini döner</returns>
        [HttpGet]
        [Route("getStories")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(IEnumerable<GetPostsRequest>))]
        public async Task<IActionResult> GetStories()
        {
            return Ok(await _storyBusinessManager.GetStories());
        }

        #endregion stories
        #region brands

        /// <summary>
        /// Markalar listesini almak için başvurulur .
        /// </summary>
        /// <returns>MArka listesini döner</returns>
        [HttpGet]
        [Route("getBrands")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(IEnumerable<GetBrandsResponse>))]
        public async Task<IActionResult> GetBrands([FromQuery] GetBrandsRequest request)
        {
            return Ok(await _brandBusinessManager.GetBrands(request));
        }

        /// <summary>
        /// Markalar listesini almak için başvurulur .
        /// </summary>
        /// <returns></returns>
        [HttpPost, DisableRequestSizeLimit]
        [Route("createBrand")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(IEnumerable<DefaultResponse>))]
        public async Task<IActionResult> CreateBrand([FromForm] CreateBrandRequest request)
        {
            var files = Request.Form.Files;
            return Ok(await _brandBusinessManager.CreateBrand(request));
        }

        /// <summary>
        /// Markalar listesini almak için başvurulur .
        /// </summary>
        /// <returns></returns>
        [HttpPost, DisableRequestSizeLimit]
        [Route("updateBrand")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(IEnumerable<DefaultResponse>))]
        public async Task<IActionResult> UpdateBrand([FromForm] UpdateBrandRequest request)
        {
            var files = Request.Form.Files;
            return Ok(await _brandBusinessManager.UpdateBrand(request));
        }

        /// <summary>
        /// Markalar silmek için kullanılır.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("deleteBrand")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(IEnumerable<DefaultResponse>))]
        public async Task<IActionResult> DeleteBrand([FromBody] DeleteBrandRequest request)
        {
            return Ok(await _brandBusinessManager.DeleteBrand(request));
        }

        #endregion brands

        #region reports

        /// <summary>
        /// Rapor listesini almak için başvurulur .
        /// </summary>
        /// <returns>TimeLine içerik listesini döner</returns>
        [HttpGet]
        [Route("getReports")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(IEnumerable<GetReportResponse>))]
        public async Task<IActionResult> GetReports()
        {
            return Ok(await _reportsBusinessManager.GetContentReports());
        }

        #endregion reports

        #region notifications
        /// <summary>
        /// Bildirim  listesini almak için başvurulur .
        /// </summary>
        /// <returns>Bildirim  listesini döner</returns>
        [HttpGet]
        [Route("GetNotifications")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(IEnumerable<GetNotificationsResponse>))]
        public async Task<IActionResult> GetNotifications()
        {
            return Ok(await _notificationBusinessManager.GetNotifications());
        }

        /// <summary>
        /// Story eklemek için kullanılır.
        /// </summary>
        /// <returns></returns>
        [HttpPost("sendNotification"), DisableRequestSizeLimit]
        public async Task<IActionResult> SendNotification([FromBody] SendNotificationRequest request)
        {
            return Ok(await _notificationBusinessManager.SendNotification(request));
        }

        #endregion notifications

        #endregion backofficeMethods
    }
}
