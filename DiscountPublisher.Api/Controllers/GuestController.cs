﻿using DiscountPublisher.Api.Helpers;
using DiscountPublisher.Business.Abstract;
using DiscountPublisher.DataTransferObjects.Base;
using DiscountPublisher.DataTransferObjects.Login;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace DiscountPublisher.Api.Controllers
{
    [Route("api/guest")]
    public class GuestController : ControllerBase
    {
        private readonly IAuthorizationsBusinessManager _authorizationsBusinessManager;
        private readonly AppSettings _appSettings;
        public GuestController(IAuthorizationsBusinessManager authorizationsBusinessManager,
                               IOptions<AppSettings> appSettings)
        {
            _authorizationsBusinessManager = authorizationsBusinessManager;
            _appSettings = appSettings.Value;
        }


        /// <summary>
        /// Sistem girişi için kullanılır.
        /// </summary>
        /// <returns></returns>
        [HttpPost("login")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(DefaultResponse<string>))]
        public async Task<IActionResult> Login([FromBody] LoginRequest request)
        {
            var result = await _authorizationsBusinessManager.Login(request);
            if (result.Success)
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                    new Claim(ClaimTypes.Name, "Rafet")
                    }),
                    Expires = DateTime.UtcNow.AddDays(365),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };
                var token = tokenHandler.CreateToken(tokenDescriptor);
                result.Record = tokenHandler.WriteToken(token);
            }
            return Ok(result);
        }

    }
}
