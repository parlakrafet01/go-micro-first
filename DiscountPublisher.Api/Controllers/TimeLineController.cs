﻿using DiscountPublisher.Business.Abstract;
using DiscountPublisher.DataTransferObjects.Base;
using DiscountPublisher.DataTransferObjects.TimeLineContent.Request;
using DiscountPublisher.DataTransferObjects.TimeLineContent.Response;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http.Description;

namespace DiscountPublisher.Api.Controllers
{
    [Route("api/timeLine")]
    public class TimeLineController : ControllerBase
    {
        private readonly ITimeLineBusinessManager _timeLineBusinessManager;
        public TimeLineController(ITimeLineBusinessManager timeLineBusinessManager)
        {
            _timeLineBusinessManager = timeLineBusinessManager;
        }

        /// <summary>
        /// Timeline listesini almak için başvurulur .
        /// </summary>
        /// <returns>TimeLine içerik listesini döner</returns>
        // GET: api/TimeLine/GetTimeLineContents
        [HttpGet]
        [Route("getTimeLineContents")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(IEnumerable<GetTimeLineContentsResponse>))]
        public async Task<IActionResult> GetTimeLineContents([FromQuery] GetTimeLineContentsRequest request)
        {
            return Ok(await _timeLineBusinessManager.GetTimeLineContents(request));
        }




        /// <summary>
        /// Görüldü bilgisi ve Yönlendirme linklerine tıklandığında bilgilendirme amaçlı burayada istek atılır.ActionType = 0 Görüldü  , 1 yönlendirme yapıldı
        /// </summary>
        /// <returns></returns>
        // GET: api/TimeLine/AddUserPostAction
        [HttpPost]
        [Route("addUserPostAction")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(DefaultResponse))]
        public async Task<IActionResult> AddUserPostAction([FromBody] UserPostActionRequest request)
        {
            return Ok(await _timeLineBusinessManager.UserPostAction(request));
        }


    
    }
}
