﻿using DiscountPublisher.Business.Abstract;
using DiscountPublisher.DataTransferObjects.Base;
using DiscountPublisher.DataTransferObjects.StoryContent.Request;
using DiscountPublisher.DataTransferObjects.StoryContent.Response;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http.Description;

namespace DiscountPublisher.Api.Controllers
{
    [Route("api/story")]
    public class StoryController : ControllerBase
    {
        private readonly IStoryBusinessManager _storyBusinessManager;
        public StoryController(IStoryBusinessManager storyBusinessManager)
        {
            _storyBusinessManager = storyBusinessManager;
        }

        /// <summary>
        /// Story listesini almak için başvurulur .
        /// </summary>
        /// <returns>Story içerik listesini döner</returns>
        // GET: api/Story/GetStoryContents
        [HttpGet]
        [Route("getStoryContents")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(IEnumerable<GetStoryContentsResponse>))]
        public async Task<IActionResult> GetStoryContents([FromQuery] GetStoryContentsRequest request)
        {
            return Ok(await _storyBusinessManager.GetStoryContents(request));
        }




        /// <summary>
        /// Görüldü bilgisi ve Yönlendirme linklerine tıklandığında bilgilendirme amaçlı burayada istek atılır.ActionType = 0 Görüldü  , 1 yönlendirme yapıldı
        /// </summary>
        /// <returns></returns>
        // GET: api/Story/AddUserStoryAction
        [HttpPost]
        [Route("addUserStoryAction")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(DefaultResponse))]
        public async Task<IActionResult> AddUserStoryAction([FromBody] UserStoryActionRequest request)
        {
            return Ok(await _storyBusinessManager.UserStoryAction(request));
        }
    }
}
