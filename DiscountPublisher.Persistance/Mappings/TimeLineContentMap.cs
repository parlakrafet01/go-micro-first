﻿using DiscountPublisher.Domain.Entity;
using MongoDB.Bson.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace DiscountPublisher.Persistance.Mappings
{
    public class TimeLineContentMap
    {
        public static void Configure()
        {
            BsonClassMap.RegisterClassMap<TimeLineContent>(map =>
            {
                map.AutoMap();
                map.SetIgnoreExtraElements(true);
                map.MapIdMember(x => x.Id);
                map.MapMember(x => x.Description);
                map.MapMember(x => x.ImagePath);
                map.MapMember(x => x.ImagePath);
                map.MapMember(x => x.ShortDescription);
            });
        }
    }
}
