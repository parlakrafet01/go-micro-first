﻿using AutoMapper;
using DiscountPublisher.Business.Abstract;
using DiscountPublisher.Business.Helpers;
using DiscountPublisher.DataTransferObjects.Base;
using DiscountPublisher.DataTransferObjects.Brand;
using DiscountPublisher.DataTransferObjects.TimeLineContent.Request;
using DiscountPublisher.DataTransferObjects.TimeLineContent.Response;
using DiscountPublisher.Domain.Entity;
using DiscountPublisher.Repository.Abstract;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace DiscountPublisher.Business.Concrete
{
    public class BrandBusinessManager : IBrandBusinessManager
    {
        private readonly IRepository<Brand> _userRepository;
        private readonly IMapper _mapper;
        private readonly string cdnUrl;
        public BrandBusinessManager(
                                       IMapper mapper,
                                       IRepository<Brand> userRepository,
                                       IConfiguration configuration)
        {
            _userRepository = userRepository;
            _mapper = mapper;
            cdnUrl = configuration["CDNUrl"];
        }

        public async Task<DefaultResponse> CreateBrand(CreateBrandRequest request)
        {
            if (request.ImageFile != null)
            {
                var hostFileName = Helper.MoveFileAndGetName(request.ImageFile);
                var model = new Brand
                {
                    Name = request.Name,
                    ImagePath = cdnUrl + hostFileName
                };

                await _userRepository.Add(model);
            }
            else
            {
                return new DefaultResponse { Success = false };
            }

            return new DefaultResponse { Success = true };
        }

        public async Task<DefaultResponse> UpdateBrand(UpdateBrandRequest request)
        {
            var data = await _userRepository.GetListByFilter(x => x.Id == ObjectId.Parse(request.Id));
            var model = data.SingleOrDefault();

            if (request.ImageFile != null)
            {
                var hostFileName = Helper.MoveFileAndGetName(request.ImageFile);
                model.ImagePath = cdnUrl + hostFileName;
            }

            model.Name = request.Name;
            var result = await _userRepository.Update(x => x.Id == ObjectId.Parse(request.Id), model);

            return new DefaultResponse { Success = result };
        }


        public async Task<DefaultResponse> DeleteBrand(DeleteBrandRequest request)
        {
            await _userRepository.Delete(x => x.Id == ObjectId.Parse(request.Id));
            return new DefaultResponse { Success = true };
        }

        public async Task<DefaultResponse<List<GetBrandsResponse>>> GetBrands(GetBrandsRequest request)
        {
            var brands = await _userRepository.GetListByFilter(x => true);
            var result = _mapper.Map<List<GetBrandsResponse>>(brands);

            return new DefaultResponse<List<GetBrandsResponse>>
            {
                Success = true,
                Record = result
            };
        }

    }
}
