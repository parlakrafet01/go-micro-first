﻿using AutoMapper;
using DiscountPublisher.Business.Abstract;
using DiscountPublisher.DataTransferObjects.Base;
using DiscountPublisher.DataTransferObjects.Notification;
using DiscountPublisher.DataTransferObjects.Report;
using DiscountPublisher.Domain.Entity;
using DiscountPublisher.Repository.Abstract;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace DiscountPublisher.Business.Concrete
{
    public class NotificationBusinessManager : INotificationBusinessManager
    {
        private readonly IRepository<Notification> _notificationContentRepository;
        private readonly IMapper _mapper;
        private readonly string fcmApiKey;
        public NotificationBusinessManager(
                                       IMapper mapper,
                                       IRepository<Notification> notificationContentRepository,
                                       IConfiguration configuration)
        {
            _notificationContentRepository = notificationContentRepository;
            _mapper = mapper;
            fcmApiKey= configuration["FCMApikey"];

        }


        public async Task<DefaultResponse<List<GetNotificationsResponse>>> GetNotifications()
        {
            var response = new DefaultResponse<List<GetNotificationsResponse>>();

            var notifications= await _notificationContentRepository.GetListByFilterOrderByDesc(x => true, 0, int.MaxValue, x => x.CreateDate);

            response.Record= _mapper.Map<List<GetNotificationsResponse>>(notifications);
            response.Success = true;

            return response;
        }

        public async Task<DefaultResponse> SendNotification(SendNotificationRequest request)
        {
            var model = new Notification
            {
                Header = request.Header,
                Body = request.Body
            };

           await  SendFcmPush(request);

            var result = await _notificationContentRepository.Add(model);

            return new DefaultResponse { Success = result };
        }

        private async Task SendFcmPush(SendNotificationRequest request)
        {
            using (var httpClient = new HttpClient())
            {
                using (var post = new HttpRequestMessage(new HttpMethod("POST"), "https://fcm.googleapis.com/fcm/send"))
                {
                    post.Headers.TryAddWithoutValidation("Authorization", "key=" + fcmApiKey + "");

                    post.Content = new StringContent("{\n    \"notification\": {\n        \"title\": \"" + request.Header + "\",\n        \"body\": \"" + request.Body + "\"\n    },\n    \"data\": {},\n    \"to\": \"/topics/all\"\n}");
                    post.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");

                    var response = await httpClient.SendAsync(post);
                }
            }
        }
    }
}
