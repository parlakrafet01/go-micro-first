﻿using AutoMapper;
using DiscountPublisher.Business.Abstract;
using DiscountPublisher.Business.Helpers;
using DiscountPublisher.DataTransferObjects.Base;
using DiscountPublisher.DataTransferObjects.StoryContent.Request;
using DiscountPublisher.DataTransferObjects.StoryContent.Response;
using DiscountPublisher.DataTransferObjects.TimeLineContent.Request;
using DiscountPublisher.Domain.Entity;
using DiscountPublisher.Repository.Abstract;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DiscountPublisher.Business.Concrete
{
    public class StoryBusinessManager : IStoryBusinessManager
    {
        private readonly IRepository<StoryContent> _storyContentRepository;
        private readonly IRepository<Brand> _userRepository;
        private readonly IRepository<UserStoryAction> _userStoryActionRepository;
        private readonly IMapper _mapper;
        private readonly string cdnUrl;
        public StoryBusinessManager(IRepository<StoryContent> storyContentRepository,
                                       IMapper mapper,
                                       IRepository<Brand> userRepository,
                                       IRepository<UserStoryAction> userStoryRepository,
                                       IConfiguration configuration)
        {
            _storyContentRepository = storyContentRepository;
            _userRepository = userRepository;
            _userStoryActionRepository = userStoryRepository;
            _mapper = mapper;
            cdnUrl = configuration["CDNUrl"];
        }
        public async Task<DefaultResponse> CreateStoryContent(CreateStoryContentRequest request)
        {
            if (request.ImageFile != null)
            {
                var hostFileName = Helper.MoveFileAndGetName(request.ImageFile);

                var date = DateTime.Now;
                var result = await _storyContentRepository.Add(new StoryContent
                {
                    Description = request.Description,
                    ImagePath = cdnUrl + hostFileName,
                    ShortDescription = request.ShortDescription,
                    Tags = request.Tags,
                    IsActive = request.IsActive,
                    StartDateTime = date,
                    EndDateTime = date.AddHours(request.LifeTime),
                    Duration = request.Duration,
                    RouteUrl = request.RouteUrl,
                    User = new Brand
                    {
                        Id = ObjectId.Parse(request.BrandId),
                        Name = request.BrandDescription,
                        ImagePath = request.BrandImagePath
                    }
                });
        
            }
            else
            {
                return new DefaultResponse { Success = false };
            }


            return new DefaultResponse { Success = true };
        }

        public async Task<DefaultResponse> UpdateStoryContent(UpdateStoryContentRequest request)
        {
            var post = await _storyContentRepository.GetListByFilter(x => x.Id == ObjectId.Parse(request.Id));
            var model = post.SingleOrDefault();

            if (request.ImageFile != null)
            {
                var hostFileName = Helper.MoveFileAndGetName(request.ImageFile);
                model.ImagePath = cdnUrl + hostFileName;
            }

            model.Description = request.Description;
            model.User.Id = ObjectId.Parse(request.BrandId);
            model.User.Name = request.BrandDescription;
            model.User.ImagePath = request.BrandImagePath;
            model.RouteUrl = request.RouteUrl;
            model.EndDateTime = model.StartDateTime.AddHours(request.LifeTime);
            model.Duration = request.Duration;


            var result = await _storyContentRepository.Update(x => x.Id == ObjectId.Parse(request.Id), model);

            return new DefaultResponse { Success = true };
        }

        public async Task<DefaultResponse> DeleteStoryContent(string Id)
        {
            await _storyContentRepository.Delete(x => x.Id == ObjectId.Parse(Id));

            return new DefaultResponse { Success = true };
        }
        public async Task<DefaultResponse> UpdateStoryContentActive(UpdatePostActiveRequest request)
        {
            var post = await _storyContentRepository.GetListByFilter(x => x.Id == ObjectId.Parse(request.Id));
            var model = post.SingleOrDefault();
            model.IsActive = request.Active;

            var result = await _storyContentRepository.Update(x => x.Id == ObjectId.Parse(request.Id), model);
            return new DefaultResponse { Success = result };
        }

        public async Task<DefaultResponse<List<GetStoriesResponse>>> GetStories()
        {
            var result = await _storyContentRepository.GetListByFilterOrderByDesc(x => 1 == 1,
                                                                                     0,
                                                                                     int.MaxValue,
                                                                                     s => s.CreateDate);
            var data = _mapper.Map<List<GetStoriesResponse>>(result);

            foreach (var item in data)
            {
                item.CreatedTime = Helper.GetDateTimeString(item.StartDateTime);
                item.LifeTime = (int)(item.EndDateTime - item.StartDateTime).TotalHours;
            }


            return new DefaultResponse<List<GetStoriesResponse>>
            {
                Success = true,
                Record = data,
                RecordCount = await _storyContentRepository.GetCountFilter(x => x.IsActive &&
                                                                               x.StartDateTime <= DateTime.Now &&
                                                                               x.EndDateTime >= DateTime.Now)
            };
        }


        public async Task<DefaultResponse<List<GetStoryContentsResponse>>> GetStoryContents(GetStoryContentsRequest request)
        {
            var result = await _storyContentRepository.GetListByFilterOrderByDesc(x => x.IsActive &&
                                                                               x.StartDateTime <= DateTime.Now &&
                                                                               x.EndDateTime >= DateTime.Now,
                                                                               request.Offset, request.Limit, 
                                                                               s=>s.CreateDate);

           var data =  result.GroupBy(x => x.User.Id).Select(x=> new GetStoryContentsResponse {
               BrandId=x.Key.ToString(),
               BrandImagePath=result.Where(p=>p.User.Id==x.Key).FirstOrDefault().User.ImagePath,
               BrandName = result.Where(p => p.User.Id == x.Key).FirstOrDefault().User.Name,
               Stories = _mapper.Map<List<GetStoryContentsResponse.StoryDto>>(x.ToList())
            }).ToList();

            foreach (var item in data)
            {
                foreach (var story in item.Stories)
                {
                    story.IsViewed = await CheckStoryIsViewed(request.DeviceId,story.Id);
                    story.CreatedTime = Helper.GetDateTimeString(story.StartDateTime);
                }
            }

            return new DefaultResponse<List<GetStoryContentsResponse>>
            {
                Success = true,
                Record = data,
                RecordCount = await _storyContentRepository.GetCountFilter(x => x.IsActive &&
                                                                               x.StartDateTime <= DateTime.Now &&
                                                                               x.EndDateTime >= DateTime.Now)
            };
        }

        private async  Task<bool> CheckStoryIsViewed(string deviceId,string storyId)
        {
            var hasAction= await _userStoryActionRepository.GetCountFilter(x => x.DeviceUserId == deviceId &&
                                                          x.StoryId == ObjectId.Parse(storyId));
            return hasAction>0;
        }

        public async Task<DefaultResponse> UserStoryAction(UserStoryActionRequest request)
        {
            foreach (var item in request.StoryIdList)
            {
                await _userStoryActionRepository.Add(new UserStoryAction
                {
                    DeviceUserId = request.DeviceId,
                    StoryId= ObjectId.Parse(item),
                    ActionType=(UserStoryAction.ActionTypeEnum)request.ActionType
                });
            }

            return new DefaultResponse { Success = true };
        }
    }
}
