﻿using AutoMapper;
using DiscountPublisher.Business.Abstract;
using DiscountPublisher.Business.Helpers;
using DiscountPublisher.DataTransferObjects.Base;
using DiscountPublisher.DataTransferObjects.TimeLineContent.Request;
using DiscountPublisher.DataTransferObjects.TimeLineContent.Response;
using DiscountPublisher.Domain.Entity;
using DiscountPublisher.Repository.Abstract;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DiscountPublisher.Business.Concrete
{
    public class TimeLineBusinessManager : ITimeLineBusinessManager
    {
        private readonly IRepository<TimeLineContent> _timeLineContentRepository;
        private readonly IRepository<Brand> _userRepository;
        private readonly IRepository<UserPostAction> _userPostActionRepository;
        private readonly IMapper _mapper;
        private readonly string cdnUrl;
        public TimeLineBusinessManager(IRepository<TimeLineContent> timeLineContentRepository,
                                       IMapper mapper,
                                       IRepository<Brand> userRepository,
                                       IRepository<UserPostAction> userPostRepository,
                                       IConfiguration configuration)
        {
            _timeLineContentRepository = timeLineContentRepository;
            _userRepository = userRepository;
            _userPostActionRepository = userPostRepository;
            _mapper = mapper;
            cdnUrl = configuration["CDNUrl"];
        }
        public async Task<DefaultResponse> CreateTimeLineContent(CreateTimeLineContentRequest request)
        {
            if (request.ImageFile != null)
            {
                var hostFileName = Helper.MoveFileAndGetName(request.ImageFile);

                var date = DateTime.Now;
                var result = await _timeLineContentRepository.Add(new TimeLineContent
                {
                    Description = request.Description,
                    ImagePath = cdnUrl + hostFileName,
                    ShortDescription = request.ShortDescription,
                    Tags = request.Tags,
                    IsActive = request.IsActive,
                    StartDateTime = date,
                    EndDateTime = date.AddHours(request.LifeTime),
                    RouteUrl = request.RouteUrl,
                    User = new Brand
                    {
                        Id = ObjectId.Parse(request.BrandId),
                        Name = request.BrandDescription,
                        ImagePath = request.BrandImagePath
                    }
                });

            }
            else
            {
                return new DefaultResponse { Success = false };
            }

           
            return new DefaultResponse { Success = true };
        }

        public async Task<DefaultResponse> UpdateTimeLineContent(UpdateTimeLineContentRequest request)
        {
            var post = await _timeLineContentRepository.GetListByFilter(x => x.Id == ObjectId.Parse(request.Id));
            var model = post.SingleOrDefault();

            if (request.ImageFile != null)
            {
                var hostFileName = Helper.MoveFileAndGetName(request.ImageFile);
                model.ImagePath = cdnUrl + hostFileName;
            }

            model.Description = request.Description;
            model.User.Id = ObjectId.Parse(request.BrandId);
            model.User.Name = request.BrandDescription;
            model.User.ImagePath = request.BrandImagePath;
            model.RouteUrl = request.RouteUrl;
            model.EndDateTime = model.StartDateTime.AddHours(request.LifeTime);


            var result = await _timeLineContentRepository.Update(x => x.Id == ObjectId.Parse(request.Id), model);


            return new DefaultResponse { Success = true };
        }

        public async Task<DefaultResponse> DeleteTimeLineContent(string Id)
        {
            await _timeLineContentRepository.Delete(x=>x.Id==ObjectId.Parse(Id));

            return new DefaultResponse { Success = true };
        }

        public async Task<DefaultResponse> UpdateTimeLineContentActive(UpdatePostActiveRequest request)
        {
            var post= await _timeLineContentRepository.GetListByFilter(x => x.Id == ObjectId.Parse(request.Id));
            var model = post.SingleOrDefault();
            model.IsActive = request.Active;

            var result= await _timeLineContentRepository.Update(x => x.Id == ObjectId.Parse(request.Id), model) ;
            return new DefaultResponse { Success = result };
        }



        public async Task<DefaultResponse<List<GetTimeLineContentsResponse>>> GetTimeLineContents(GetTimeLineContentsRequest request)
        {
            var result = await _timeLineContentRepository.GetListByFilterOrderByDesc(x => x.IsActive &&
                                                                               x.StartDateTime <= DateTime.Now &&
                                                                               x.EndDateTime >= DateTime.Now,
                                                                               request.Offset, request.Limit,
                                                                               s => s.CreateDate);
            var data = _mapper.Map<List<GetTimeLineContentsResponse>>(result);

            foreach (var item in data)
            {
                item.CreatedTime = Helper.GetDateTimeString(item.StartDateTime);
                item.IsViewed = await CheckPostIsViewed(request.DeviceId, item.Id);
            }


            return new DefaultResponse<List<GetTimeLineContentsResponse>>
            {
                Success = true,
                Record = data,
                RecordCount = await _timeLineContentRepository.GetCountFilter(x => x.IsActive &&
                                                                               x.StartDateTime <= DateTime.Now &&
                                                                               x.EndDateTime >= DateTime.Now)
            };
        }

        private async Task<bool> CheckPostIsViewed(string deviceId, string postId)
        {
            var hasAction = await _userPostActionRepository.GetCountFilter(x => x.DeviceUserId == deviceId &&
                                                           x.PostId == ObjectId.Parse(postId));
            return hasAction > 0;
        }


        public async Task<DefaultResponse> UserPostAction(UserPostActionRequest request)
        {
            foreach (var item in request.PostIdList)
            {
                await _userPostActionRepository.Add(new UserPostAction
                {
                    DeviceUserId = request.DeviceId,
                    PostId = ObjectId.Parse(item),
                    ActionType = (UserPostAction.ActionTypeEnum)request.ActionType
                });
            }

            return new DefaultResponse { Success = true };
        }
        #region backofficemethods
        public async Task<DefaultResponse<List<GetPostsResponse>>> GetPosts(GetPostsRequest request)
        {


            var result = await _timeLineContentRepository.GetListByFilterOrderByDesc(x =>1==1,
                                                                                     0,
                                                                                     int.MaxValue,
                                                                                     s => s.CreateDate);
            var data = _mapper.Map<List<GetPostsResponse>>(result);

            foreach (var item in data)
            {
                item.CreatedTime = Helper.GetDateTimeString(item.StartDateTime);
                item.LifeTime = (int)(item.EndDateTime - item.StartDateTime).TotalHours;
            }


            return new DefaultResponse<List<GetPostsResponse>>
            {
                Success = true,
                Record = data,
                RecordCount = await _timeLineContentRepository.GetCountFilter(x => x.IsActive &&
                                                                               x.StartDateTime <= DateTime.Now &&
                                                                               x.EndDateTime >= DateTime.Now)
            };
        }
        #endregion backofficemethods

    }
}
