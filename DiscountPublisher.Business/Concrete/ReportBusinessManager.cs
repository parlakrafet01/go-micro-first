﻿using AutoMapper;
using DiscountPublisher.Business.Abstract;
using DiscountPublisher.DataTransferObjects.Base;
using DiscountPublisher.DataTransferObjects.Login;
using DiscountPublisher.DataTransferObjects.Report;
using DiscountPublisher.Domain.Entity;
using DiscountPublisher.Repository.Abstract;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace DiscountPublisher.Business.Concrete
{
    public class ReportBusinessManager : IReportBusinessManager
    {
        private readonly IRepository<TimeLineContent> _timeLineContentRepository;
        private readonly IRepository<StoryContent> _storyRepository;
        private readonly IRepository<UserPostAction> _userPostActionRepository;
        private readonly IRepository<UserStoryAction> _userStoryActionRepository;
        private readonly IMapper _mapper;
        private readonly string cdnUrl;
        public ReportBusinessManager(
                                       IMapper mapper,
                                       IRepository<TimeLineContent> timeLineContentRepository,
                                       IRepository<StoryContent> storyRepository,
                                       IRepository<UserPostAction> userPostActionRepository,
                                       IRepository<UserStoryAction> userStoryActionRepository,
                                       IConfiguration configuration)
        {
            _timeLineContentRepository = timeLineContentRepository;
            _storyRepository = storyRepository;
            _userPostActionRepository = userPostActionRepository;
            _userStoryActionRepository = userStoryActionRepository;
            _mapper = mapper;
            cdnUrl = configuration["CDNUrl"];
        }

        public async Task<GetReportResponse> GetContentReports()
        {
            var response = new GetReportResponse();
            var posts = await _timeLineContentRepository.GetListByFilter(x => true);

            foreach (var post in posts)
            {
                var viewCount = await _userPostActionRepository.GetCountFilter(x => x.PostId == post.Id &&
                                                                                    x.ActionType == UserPostAction.ActionTypeEnum.Viewed);

                var clickCount = await _userPostActionRepository.GetCountFilter(x => x.PostId == post.Id &&
                                                                                     x.ActionType == UserPostAction.ActionTypeEnum.Clicked);
                var item = new GetReportResponse.ItemDto()
                {
                    Description = post.Description,
                    ImagePath = post.ImagePath,
                    Id = post.Id.ToString(),
                    RouteCount = clickCount,
                    ViewCount = viewCount
                };
                response.Posts.Add(item);
            }

            var stories = await _storyRepository.GetListByFilter(x => true);

            foreach (var story in stories)
            {
                var viewCount = await _userStoryActionRepository.GetCountFilter(x => x.StoryId == story.Id &&
                                                                                    x.ActionType == UserStoryAction.ActionTypeEnum.Viewed);

                var clickCount = await _userStoryActionRepository.GetCountFilter(x => x.StoryId == story.Id &&
                                                                                     x.ActionType == UserStoryAction.ActionTypeEnum.Clicked);
                var item = new GetReportResponse.ItemDto()
                {
                    Description = story.Description,
                    ImagePath = story.ImagePath,
                    Id = story.Id.ToString(),
                    RouteCount = clickCount,
                    ViewCount = viewCount
                };
                response.Stories.Add(item);
            }


            return response;
        }
    }

    public class AuthorizationsBusinessManager : IAuthorizationsBusinessManager
    {
        private readonly IRepository<Authentications> _authenticationsRepository;
        private readonly IMapper _mapper;
        private readonly string cdnUrl;
        public AuthorizationsBusinessManager(
                                       IMapper mapper,
                                       IRepository<Authentications> authenticationsRepository)
        {
            _authenticationsRepository = authenticationsRepository;
            _mapper = mapper;
        }

        public async Task<DefaultResponse<string>> Login(LoginRequest request)
        {
            var auths= await _authenticationsRepository.GetListByFilter(x => x.Username == request.Username && x.Password == request.Password);

            if (auths.Count>0)
            {
                return new DefaultResponse<string> { Success = true };
            }
            else
            {
                return new DefaultResponse<string> { Success = false };
            }

        }
    }
}
