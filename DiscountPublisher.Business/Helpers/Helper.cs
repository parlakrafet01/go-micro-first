﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http.Headers;
using System.Text;

namespace DiscountPublisher.Business.Helpers
{
    public static class Helper
    {
        public static string GetDateTimeString(DateTime date)
        {
            date = date.ToLocalTime();
            var diff = DateTime.Now - date;
            var minutes = diff.Minutes;
            var hours = diff.Hours;
            var days = diff.Days;

            if (days>0)
            {
                return days.ToString() + "g";
            }
            else if (hours>0)
            {
                return hours.ToString() + "s";
            }
            else if (minutes > 0)
            {
                return minutes.ToString() + "d";
            }
            else
            {
                return  "1d";
            }

        }

        public static string MoveFileAndGetName(IFormFile formFile)
        {
            
            var filename = ContentDispositionHeaderValue
                                            .Parse(formFile.ContentDisposition)
                                            .FileName
                                            .Trim('"');

            var hostFileName = filename.Split('.')[0] + DateTime.Now.ToString("yyyyyMMddHHmmss") + "." + filename.Split('.')[1];
            filename = @"C:\CDN" + $@"\{hostFileName}";
            using (FileStream fs = System.IO.File.Create(filename))
            {
                formFile.CopyTo(fs);
                fs.Flush();
            }

            return hostFileName;
        }
    }
}
