﻿using DiscountPublisher.DataTransferObjects.Report;
using System.Threading.Tasks;

namespace DiscountPublisher.Business.Abstract
{
    public interface IReportBusinessManager
    {
        Task<GetReportResponse> GetContentReports();
    }
}
