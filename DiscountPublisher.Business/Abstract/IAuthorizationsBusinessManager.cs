﻿using DiscountPublisher.DataTransferObjects.Base;
using DiscountPublisher.DataTransferObjects.Login;
using System.Threading.Tasks;

namespace DiscountPublisher.Business.Abstract
{
    public interface IAuthorizationsBusinessManager
    {
        Task<DefaultResponse<string>> Login(LoginRequest request);
    }
}
