﻿using DiscountPublisher.DataTransferObjects.Base;
using DiscountPublisher.DataTransferObjects.TimeLineContent.Request;
using DiscountPublisher.DataTransferObjects.TimeLineContent.Response;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DiscountPublisher.Business.Abstract
{
    public interface ITimeLineBusinessManager
    {
        Task<DefaultResponse> CreateTimeLineContent(CreateTimeLineContentRequest request);
        Task<DefaultResponse> UpdateTimeLineContentActive(UpdatePostActiveRequest request);
        Task<DefaultResponse<List<GetTimeLineContentsResponse>>> GetTimeLineContents(GetTimeLineContentsRequest request);

        Task<DefaultResponse> UserPostAction(UserPostActionRequest request);

        Task<DefaultResponse<List<GetPostsResponse>>> GetPosts(GetPostsRequest request);
        Task<DefaultResponse> UpdateTimeLineContent(UpdateTimeLineContentRequest request);
        Task<DefaultResponse> DeleteTimeLineContent(string Id);
    }
}
