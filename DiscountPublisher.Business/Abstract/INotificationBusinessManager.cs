﻿using DiscountPublisher.DataTransferObjects.Base;
using DiscountPublisher.DataTransferObjects.Notification;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DiscountPublisher.Business.Abstract
{
    public interface INotificationBusinessManager
    {
        Task<DefaultResponse> SendNotification(SendNotificationRequest request);
        Task<DefaultResponse<List<GetNotificationsResponse>>> GetNotifications();

    }
}
