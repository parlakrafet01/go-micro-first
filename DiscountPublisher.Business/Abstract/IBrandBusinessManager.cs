﻿using DiscountPublisher.DataTransferObjects.Base;
using DiscountPublisher.DataTransferObjects.Brand;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DiscountPublisher.Business.Abstract
{
    public interface IBrandBusinessManager
    {
        Task<DefaultResponse> CreateBrand(CreateBrandRequest request);
        Task<DefaultResponse> DeleteBrand(DeleteBrandRequest request);
        Task<DefaultResponse<List<GetBrandsResponse>>> GetBrands(GetBrandsRequest request);
        Task<DefaultResponse> UpdateBrand(UpdateBrandRequest request);
    }
}
