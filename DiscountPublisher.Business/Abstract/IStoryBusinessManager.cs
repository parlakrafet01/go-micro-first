﻿using DiscountPublisher.DataTransferObjects.Base;
using DiscountPublisher.DataTransferObjects.StoryContent.Request;
using DiscountPublisher.DataTransferObjects.StoryContent.Response;
using DiscountPublisher.DataTransferObjects.TimeLineContent.Request;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DiscountPublisher.Business.Abstract
{
    public interface IStoryBusinessManager
    {
        Task<DefaultResponse> CreateStoryContent(CreateStoryContentRequest request);

        Task<DefaultResponse> UpdateStoryContent(UpdateStoryContentRequest request);
        Task<DefaultResponse> DeleteStoryContent(string Id);
        Task<DefaultResponse> UpdateStoryContentActive(UpdatePostActiveRequest request);

        Task<DefaultResponse<List<GetStoryContentsResponse>>> GetStoryContents(GetStoryContentsRequest request);

        Task<DefaultResponse<List<GetStoriesResponse>>> GetStories();

        Task<DefaultResponse> UserStoryAction(UserStoryActionRequest request);
    }
}
