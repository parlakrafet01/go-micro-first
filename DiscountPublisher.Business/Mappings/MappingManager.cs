﻿using AutoMapper;
using DiscountPublisher.DataTransferObjects.Brand;
using DiscountPublisher.DataTransferObjects.Notification;
using DiscountPublisher.DataTransferObjects.StoryContent.Response;
using DiscountPublisher.DataTransferObjects.TimeLineContent.Response;
using DiscountPublisher.Domain.Entity;
using System.Collections.Generic;

namespace DiscountPublisher.Business.Mappings
{
    public class MappingManager : Profile
    {
        public MappingManager()
        {

            CreateMap<TimeLineContent, GetTimeLineContentsResponse>()
                .ForMember(p => p.CreatedTime, x => x.Ignore())
                .ForMember(p => p.BrandId, x => x.MapFrom(c => c.User.Id.ToString()))
                .ForMember(p => p.BrandName, x => x.MapFrom(c => c.User.Name))
                .ForMember(p => p.BrandImagePath, x => x.MapFrom(c => c.User.ImagePath));

            CreateMap<TimeLineContent, GetPostsResponse>()
               .ForMember(p => p.BrandId, x => x.MapFrom(c => c.User.Id.ToString()))
               .ForMember(p => p.BrandName, x => x.MapFrom(c => c.User.Name))
               .ForMember(p => p.BrandImagePath, x => x.MapFrom(c => c.User.ImagePath));

            CreateMap<StoryContent, GetStoryContentsResponse.StoryDto>()
               .ForMember(p => p.CreatedTime, x => x.Ignore());

            CreateMap<StoryContent, GetStoriesResponse>()
               .ForMember(p => p.BrandId, x => x.MapFrom(c => c.User.Id.ToString()))
               .ForMember(p => p.BrandName, x => x.MapFrom(c => c.User.Name))
               .ForMember(p => p.BrandImagePath, x => x.MapFrom(c => c.User.ImagePath));

            CreateMap<Brand, GetBrandsResponse>()
               .ForMember(p => p.ImageUrl, x => x.MapFrom(c => c.ImagePath));


            CreateMap<Notification, GetNotificationsResponse>();

        }
    }
}
