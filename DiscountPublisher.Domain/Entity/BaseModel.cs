﻿using System;

namespace DiscountPublisher.Domain.Entity
{
    public abstract class BaseModel
    {
        public DateTime  CreateDate { get; set; }
    }
}
