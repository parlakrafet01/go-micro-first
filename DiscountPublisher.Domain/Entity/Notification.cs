﻿using MongoDB.Bson;

namespace DiscountPublisher.Domain.Entity
{
    public class Notification : BaseModel
    {
        public ObjectId Id { get; set; }
        public string Header { get; set; }
        public string Body { get; set; }
    }
}
