﻿using MongoDB.Bson;

namespace DiscountPublisher.Domain.Entity
{
    public class Brand :BaseModel
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public string ImagePath { get; set; }
    }
}
