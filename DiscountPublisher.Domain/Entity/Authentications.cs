﻿using MongoDB.Bson;

namespace DiscountPublisher.Domain.Entity
{
    public class Authentications
    {
        public ObjectId Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
