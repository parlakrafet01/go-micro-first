﻿using MongoDB.Bson;

namespace DiscountPublisher.Domain.Entity
{
    public class UserStoryAction : BaseModel
    {
        public ObjectId Id { get; set; }
        public string DeviceUserId { get; set; }
        public ObjectId StoryId { get; set; }
        public ActionTypeEnum ActionType { get; set; }
        public enum ActionTypeEnum
        {
            Viewed,
            Clicked
        }
    }
}
