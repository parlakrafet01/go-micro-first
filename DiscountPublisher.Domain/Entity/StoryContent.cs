﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;

namespace DiscountPublisher.Domain.Entity
{
    public class StoryContent : BaseModel
    {
        public ObjectId Id { get; set; }
        public string Description { get; set; }
        public string ShortDescription { get; set; }
        public string ImagePath { get; set; }
        public string RouteUrl { get; set; }
        public List<string> Tags { get; set; }
        public bool IsActive { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public int Duration { get; set; }
        public Brand User { get; set; }
        public int ViewCount { get; set; }
        public int RouteCount { get; set; }
    }
}
