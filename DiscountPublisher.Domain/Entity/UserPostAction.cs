﻿using MongoDB.Bson;

namespace DiscountPublisher.Domain.Entity
{
    public class UserPostAction : BaseModel
    {
        public ObjectId Id { get; set; }
        public string DeviceUserId { get; set; }
        public ObjectId PostId { get; set; }
        public ActionTypeEnum ActionType { get; set; }
        public enum ActionTypeEnum{
            Viewed,
            Clicked
        }
    }
}
