﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DiscountPublisher.DataTransferObjects.TimeLineContent.Response
{
    public class GetTimeLineContentsResponse
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public string ImagePath { get; set; }
        public string CreatedTime { get; set; }
        public DateTime StartDateTime { get; set; }
        public string RouteUrl { get; set; }
        public List<string> Tags { get; set; }
        public string BrandId { get; set; }
        public string BrandName { get; set; }
        public string BrandImagePath { get; set; }
        public bool IsViewed { get; set; }

    }


}
