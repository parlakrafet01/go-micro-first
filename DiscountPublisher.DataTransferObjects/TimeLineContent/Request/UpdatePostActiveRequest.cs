﻿namespace DiscountPublisher.DataTransferObjects.TimeLineContent.Request
{
    public class UpdatePostActiveRequest
    {
        public string Id { get; set; }
        public bool Active { get; set; }
    }
}