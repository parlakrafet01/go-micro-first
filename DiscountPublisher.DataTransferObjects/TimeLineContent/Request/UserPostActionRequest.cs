﻿using System.Collections.Generic;

namespace DiscountPublisher.DataTransferObjects.TimeLineContent.Request
{
    public class UserPostActionRequest
    {
        public string DeviceId { get; set; }
        public List<string> PostIdList { get; set; }
        public int ActionType { get; set; }
    }
}
