﻿using System.Text;

namespace DiscountPublisher.DataTransferObjects.TimeLineContent.Request
{
    public class GetTimeLineContentsRequest
    {
        public int Offset { get; set; }
        public int Limit { get; set; }
        public string DeviceId { get; set; }
    }
}
