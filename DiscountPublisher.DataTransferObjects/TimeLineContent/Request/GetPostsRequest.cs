﻿using System;

namespace DiscountPublisher.DataTransferObjects.TimeLineContent.Request
{
    public class GetPostsRequest
    {
        public string SearchKeyword { get; set; }
        public DateTime? StartDateTime { get; set; }
        public DateTime? EndDateTime { get; set; }
    }
}
