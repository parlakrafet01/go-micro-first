﻿using System.Text;

namespace DiscountPublisher.DataTransferObjects.StoryContent.Request
{
    public class GetStoryContentsRequest
    {
        public int Offset { get; set; }
        public int Limit { get; set; }
        public string DeviceId { get; set; }
    }
}
