﻿using System.Collections.Generic;

namespace DiscountPublisher.DataTransferObjects.StoryContent.Request
{
    public class UserStoryActionRequest
    {
        public string DeviceId { get; set; }
        public List<string> StoryIdList { get; set; }
        public int ActionType { get; set; }
    }
}
