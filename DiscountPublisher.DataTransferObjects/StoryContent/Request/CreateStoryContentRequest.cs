﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace DiscountPublisher.DataTransferObjects.StoryContent.Request
{
    public class CreateStoryContentRequest
    {
        public string Description { get; set; }
        public string ShortDescription { get; set; }
        public string ImagePath { get; set; }
        public IFormFile ImageFile { get; set; }
        public string RouteUrl { get; set; }
        public List<string> Tags { get; set; }
        public bool IsActive { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public int Duration { get; set; }
        public string BrandId { get; set; }
        public string BrandDescription { get; set; }
        public string BrandImagePath { get; set; }
        public int LifeTime { get; set; }
    }
}

