﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DiscountPublisher.DataTransferObjects.StoryContent.Response
{
    public class GetStoryContentsResponse
    {

        public string BrandId { get; set; }
        public string BrandName { get; set; }
        public string BrandImagePath { get; set; }

        public class StoryDto
        {
            public string Id { get; set; }
            public string Description { get; set; }
            public string ImagePath { get; set; }
            public string CreatedTime { get; set; }
            public DateTime StartDateTime { get; set; }
            public string RouteUrl { get; set; }
            public List<string> Tags { get; set; }
            public int Duration { get; set; }
            public bool IsViewed { get; set; }
        }

        public List<StoryDto> Stories { get; set; }


    }

}
