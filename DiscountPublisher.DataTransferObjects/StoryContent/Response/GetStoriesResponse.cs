﻿using System;
using System.Collections.Generic;

namespace DiscountPublisher.DataTransferObjects.StoryContent.Response
{
    public class GetStoriesResponse
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public string ImagePath { get; set; }
        public string CreatedTime { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public DateTime CreateDate { get; set; }
        public string RouteUrl { get; set; }
        public List<string> Tags { get; set; }
        public string BrandId { get; set; }
        public string BrandName { get; set; }
        public string BrandImagePath { get; set; }
        public bool IsActive { get; set; }
        public int LifeTime { get; set; }
        public int Duration { get; set; }
    }

}
