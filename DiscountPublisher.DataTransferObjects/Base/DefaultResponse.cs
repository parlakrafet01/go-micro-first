﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DiscountPublisher.DataTransferObjects.Base
{
    public class DefaultResponse<T>
    {
        public bool Success { get; set; }
        public T Record { get; set; }
        public string Message { get; set; }
        public long RecordCount { get; set; }
    }

    public class DefaultResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}
