﻿
namespace DiscountPublisher.DataTransferObjects.Brand
{
    public class GetBrandsResponse
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
    }
}
