﻿
using Microsoft.AspNetCore.Http;

namespace DiscountPublisher.DataTransferObjects.Brand
{
    public class CreateBrandRequest
    {
        public string Name { get; set; }
        public string ImagePath { get; set; }
        public IFormFile ImageFile { get; set; }
    }
}
