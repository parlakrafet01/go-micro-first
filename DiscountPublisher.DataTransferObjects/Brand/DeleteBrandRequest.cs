﻿
namespace DiscountPublisher.DataTransferObjects.Brand
{
    public class DeleteBrandRequest
    {
        public string Id { get; set; }
    }

}
