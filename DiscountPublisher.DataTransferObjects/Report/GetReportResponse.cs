﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DiscountPublisher.DataTransferObjects.Report
{
    public class GetReportResponse
    {
        public GetReportResponse()
        {
            Posts = new List<ItemDto>();
            Stories = new List<ItemDto>();
        }

        public List<ItemDto> Posts { get; set; }
        public List<ItemDto> Stories { get; set; }

        public class ItemDto
        {
            public string Id { get; set; }
            public string ImagePath { get; set; }
            public string Description { get; set; }
            public string RouteUrl { get; set; }
            public long ViewCount { get; set; }
            public long RouteCount { get; set; }
        }
    }
}
