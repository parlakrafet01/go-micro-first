﻿using System;

namespace DiscountPublisher.DataTransferObjects.Notification
{
    public class GetNotificationsResponse
    {
        public string Header { get; set; }
        public string Body { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
