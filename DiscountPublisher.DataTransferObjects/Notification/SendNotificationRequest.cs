﻿using System.Collections.Generic;
using System.Text;

namespace DiscountPublisher.DataTransferObjects.Notification
{
    public class SendNotificationRequest
    {
        public string Header { get; set; }
        public string Body { get; set; }
    }
}
